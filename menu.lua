-----------------------------------------------------------------------------------------
--
-- menu.lua
--
-----------------------------------------------------------------------------------------

local composer = require( "composer" )
local scene = composer.newScene()

-- include Corona's "widget" library
local widget = require "widget"

-- include "json" library
json = require('json')

--------------------------------------------


local playBtn

-- 'onRelease' event listener for playBtn
local function onPlayBtnRelease()
	if composer.getScene("refill") == nil then --wait untill refill scene is destroyed
		if composer.getScene("gameover") == nil then --wait untill gameover scene is destroyed
			if composer.getScene("game") == nil then --wait untill game scene is destroyed
			
				settingsButtonHide()
			
				-- subtract life
				data.currentplayerlives = data.currentplayerlives - 1
				
				-- save changes to data table
				saveValue('data.txt', json.encode(data))
			
				-- animate button
				transition.to( playBtn, { time=1, alpha=0 } )
				transition.to( settingsBtn, { time=100, alpha=0 } )
				transition.to( logoGroup, { time=200, alpha=0 } )
				transition.to( logoGroup, { time=200, y=-halfW*0.1, transition=easing.inExpo} )
				transition.to( playBtnAni, { delay=100, time=200, alpha=1, height=playersize, width=playersize, x=halfW-playersize*0.5, y=screenH/3*2-playersize*0.5, transition=inOutExpo } )
				
				-- go to game scene
				timer.performWithDelay( 300, function() composer.gotoScene("game"); end)
			end
		end
	end
	return true	-- indicates successful touch
end


function scene:create( event )
	local sceneGroup = self.view

	-- Called when the scene's view does not exist.
	-- 
	-- INSERT code here to initialize the scene
	-- e.g. add display objects to 'sceneGroup', add touch listeners, etc.
	
	-- logo
	logoGroup = display.newGroup()
	logoGroup.anchorX = 0
	logoGroup.anchorY = 0
	logoGroup.x = 0
	logoGroup.y = 0
	logoGroup.alpha = 1
	
	logo = display.newImageRect("logo.png", 200, 64 )
	logo.x, logo.y = halfW, (screenH/3)
	logoGroup:insert(logo)
	
	logodot = display.newRect( logo.x-19.9, logo.y-14, 10, 10)
	logodot:setFillColor( getColor("accentcolor") )
	logoGroup:insert(logodot)
	

	-- widget start button (loads game.lua on release)
	playBtn = widget.newButton{
		label="GO",
		font=fontface,
		fontSize = 28,
		labelYOffset = 1,
		shape="Rect",
		emboss = false,
		cornerRadius = 2,
		labelColor = { default={ getColor("forecolor") }, over={ getColor("bgcolor") } },
		fillColor = { default={ getColor("accentcolor") }, over={ getColor("accentcolor") } },
		width=buttonsize, height=buttonsize,
		onRelease = onPlayBtnRelease	-- event listener function
	}
	playBtn.x = display.contentWidth*0.5
	playBtn.y = display.contentHeight/3*2
	
	playBtnAni = display.newRect( 0, 0, buttonsize, buttonsize)
	playBtnAni:setFillColor( getColor("accentcolor") )
	playBtnAni.anchorX = 0
	playBtnAni.anchorY = 0
	playBtnAni.x = playBtn.x - buttonsize*0.5
	playBtnAni.y = playBtn.y - buttonsize*0.5

	
	-- all display objects must be inserted into group
	sceneGroup:insert( logoGroup )
	sceneGroup:insert( playBtnAni )
	sceneGroup:insert( playBtn )
	--sceneGroup:insert( settingsBtn )
end

function scene:show( event )
	local sceneGroup = self.view
	local phase = event.phase
	
	if phase == "will" then
		-- Called when the scene is still off screen and is about to move on screen
	elseif phase == "did" then
		-- Called when the scene is now on screen
		-- 
		-- INSERT code here to make the scene come alive
		-- e.g. start timers, begin animation, play audio, etc.
		
		timer.performWithDelay( 10, function() composer.removeScene("refill"); end)
		
	end	
end

function scene:hide( event )
	local sceneGroup = self.view
	local phase = event.phase
	
	if event.phase == "will" then
		-- Called when the scene is on screen and is about to move off screen
		--
		-- INSERT code here to pause the scene
		-- e.g. stop timers, stop animation, unload sounds, etc.)
	elseif phase == "did" then
		-- Called when the scene is now off screen
	end	
end

function scene:destroy( event )
	local sceneGroup = self.view
	
	-- Called prior to the removal of scene's "view" (sceneGroup)
	-- 
	-- INSERT code here to cleanup the scene
	-- e.g. remove display objects, remove touch listeners, save state, etc.
	
	if playBtn then
		playBtn:removeSelf()	-- widgets must be manually removed
		playBtn = nil
	end
	
	if settingsBtn then
		settingsBtn:removeSelf()	-- widgets must be manually removed
		settingsBtn = nil
	end 
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-----------------------------------------------------------------------------------------

return scene