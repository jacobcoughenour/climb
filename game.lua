-----------------------------------------------------------------------------------------
--
-- game.lua
--
-----------------------------------------------------------------------------------------

local composer = require( "composer" )
local scene = composer.newScene()

-- include Corona's "physics" library
local physics = require "physics"
physics.start(); physics.pause()

-- include "json" library
json = require('json')

-- debug
physics.setDrawMode( "normal" )

-----------------
-- Variables --
-----------------

-- static dimensions/settings
local linewidth = 4
local flipbias = 4 -- creates a poping animation
local linespeedmax = 8

-- dynamic stuff 
local linespeed = 2.5 -- start speed
local lineaccel = 1
local playergravity = linespeed * 100
local lineheight = 160
local gameStarted = false
local currentside = -1   -- -1 is right, 1 is left
local currentscoretime = 0
local finalscoretime = 0


----------------------
-- Scene Create --
----------------------

function scene:create( event )

	-- Called when the scene's view does not exist.
	--
	-- INSERT code here to initialize the scene
	-- e.g. add display objects to 'sceneGroup', add touch listeners, etc.

	local sceneGroup = self.view
	
	-- line group
	lineGroup = display.newGroup()
	lineGroup.anchorX = 0
	lineGroup.anchorY = 0
	lineGroup.x = 0
	lineGroup.y = 0
	lineGroup.alpha = 0
	

	-- player square
	player = display.newRect( 0, 0, playersize, playersize )
	player:setFillColor( getColor("accentcolor") )
	player.x, player.y = halfW, screenH-screenH/3 -- player stating position
	player.rotation = 0
	player.collision = onPlayerCollision

	-- player piston
	playerpiston = display.newRect (0, 0, 2, 2)
	playerpiston:setFillColor( getColor("forecolor") )
	playerpiston.x, playerpiston.y = -4, screenH-screenH/3 -- offscreen
	playerpiston.rotation = 0

	-- add physics to the player
	physics.addBody( player, "dynamic", { density=0, friction=0, bounce=0 } )
	physics.addBody( playerpiston, "static" )
	player.isFixedRotation = true -- player can't rotate/spin
	player.isSleepingAllowed = false -- physics stay awake

	physics.setGravity( -playergravity, 0)

	-- add joint between player and playerpiston
	-- to keep player from going up or down
	local playerjoint =  physics.newJoint( "piston", player, playerpiston, playerpiston.x, playerpiston.y, 1, 0 )
	playerjoint.isMotorEnabled = false
	playerjoint.isLimitEnabled = false
	
	-- score clock background
	local scoreClockBackground = display.newRect(0, 0, screenW, halfH*0.25)
	scoreClockBackground.fill = {type = "gradient", color1 = { getColor("bgcolor") }, color2 = { 0,0 }, direction = "down"} -- gradient
	scoreClockBackground.anchorX = 0
	scoreClockBackground.anchorY = 0
	scoreClockBackground.x = 0
	scoreClockBackground.y = 0	
	

	-- score clock
	scoreClockText = display.newText("0", halfW, (screenH*0.125)*0.5, fontface, 40)
	scoreClockText:setFillColor( getColor("accentcolor") )
	
	
	-- all display objects must be inserted into group
	sceneGroup:insert( lineGroup )
	sceneGroup:insert( player )
	sceneGroup:insert( scoreClockBackground )
	sceneGroup:insert( scoreClockText )
end


-------------------
-- Score Clock --
-------------------

currentscoretime = 0

local function updateTime()
	if gameStarted == true then
		-- decrement the number of seconds
	
	-- compares current time to start time
	--local previousTime = os.time()
	--local currentscoretime = os.time() - starttime
	--print( currentscoretime, previousTime, starttime )
	
	-- simple method that removes the 12:00 issue
	currentscoretime = currentscoretime + 1
	
	-- make it a string using string format.  
	local timeDisplay = string.format( "%01d", currentscoretime)
	scoreClockText.text = timeDisplay
	
	finalscoretime = currentscoretime
	
	elseif gameStarted == false then
		
		finalscoretime = currentscoretime
		
	end
end

------------------------
-- Line Movement --
------------------------

local function moveLine()
	for a = lineGroup.numChildren,1,-1  do

		-- if the line is still in the top of the screen
		if lineGroup[a].y >=  -linespeed then -- "-linespeed" removes the gap between duplicates
			if lineGroup[a].intop == true then 
				createLine()
				lineGroup[a].intop = false
			end
		end
			
		-- destroy line after leaving screen
		if lineGroup[a].y > (screenH+8)  then
			lineGroup:remove(lineGroup[a]) -- i think this works...
		else 
			lineGroup[a].y = lineGroup[a].y + linespeed -- move line down
		end 
	end
	-- speed up over time
	if linespeed < linespeedmax then -- speed max
		linespeed = linespeed + lineaccel*0.001
	else
		linespeed = linespeedmax
	end
	playergravity = linespeed * linespeed*0.5 * 64 -- add to the gravity to makeup for speed
end


------------------------
-- Line Generation --
------------------------

local firstline = true
local extralinespace = screenW/24
local lineoffset = extralinespace*0.5+extralinespace
local linecolor = getColor("forecolor")

function createLine()
	if firstline == true then 
		-- first line
		height = (screenH + 20)
		linex = (halfW)+(playersize+linewidth+extralinespace)*0-(linewidth+(playersize*0.5)+extralinespace*2)+lineoffset
		liney = -20
	else 
		-- random line generation
		height = (math.random( 16, 28 ) * playersize*0.25)
		linex = (halfW)+(playersize+linewidth+extralinespace)*(math.random( -2, 3))-(linewidth+(playersize*0.5)+extralinespace*2)+lineoffset
		liney = -height+linespeed
	end
	if gameStarted == true then -- stop making lines on player death
		-- create line
		line = display.newRect( 0, 0, linewidth, height )
		line:setFillColor( linecolor )
		line.anchorX = 0
		line.anchorY = 0
		line.x = linex
		line.y = liney
		physics.addBody( line, "static", { friction=0 } )
		
		line.intop = true -- used for moveLine()
		lineGroup:insert(line)
		
		firstline = false
	end
end

---------------------
-- Switch Sides --
---------------------

local function SwitchSides(event)
	if event.phase == "began" then
		if gameStarted == true then
		
		local vx, vy = player:getLinearVelocity()
		
			if vx>=-flipbias and vx<=flipbias then -- 
			
				-- move player to the other side
				player.x = player.x + ((playersize + linewidth + flipbias) * currentside)
				
				-- swap gravity 
				physics.setGravity((-playergravity * currentside), 0)
				
				-- play sound
				if settings.soundmode == 1 then
					audio.play( sounds["swap"] )
				end
				
				-- set currentside for next time
				if currentside == 1 then -- if it was on the left
					currentside = -1
					-- set to right
				else -- if it was on the right side
					currentside = 1 -- set to left
				end
			end
		end
	end
end


---------------------
-- Player Death --
---------------------

local function playerdeath()
	if gameStarted == true then
		-- when player has left the screen
		if player.x > screenW+playersize or player.x < -playersize then
			gameStarted = false
			
			composer.setVariable( "gameoverscore", finalscoretime )
			
			--fade out lines
			transition.to( lineGroup, { delay=0, time=500, alpha=0 })
			
			-- load gameover scene
			composer.gotoScene("gameover", "crossFade", 300)
			
		end
	end
end

---------------
-- Sounds --
---------------

local function onPlayerCollision( self, event )
    if ( event.force >= playergravity*0.0003 ) then
		if settings.soundmode == 1 then
			-- sound 
			audio.play( sounds["bork"] )
		end
		if settings.vibratemode == 1 then
			system.vibrate()
		end
	end
end

------------------
-- player color --
------------------

local function playerColor(event)
	if gameStarted then
		local rand = math.random( 1, 3)
		if rand == 1 then
			player:setFillColor( 33/255, 150/255, 243/255 )
		elseif rand == 2 then
			player:setFillColor( 239/255, 61/255, 55/255 )
		elseif rand == 3 then
			player:setFillColor( 139/255, 195/255, 74/255 )
		end
	else
		timer.cancel( event.source )
	end
end


--------------------
-- Scene Show --
--------------------

function scene:show( event )
	local sceneGroup = self.view
	local phase = event.phase

	if phase == "will" then
		-- Called when the scene is still off screen and is about to move on screen
	elseif phase == "did" then
		-- Called when the scene is now on screen
		--
		-- INSERT code here to make the scene come alive
		-- e.g. start timers, begin animation, play audio, etc.
		physics.start()
		
		
		-- player wobble animation
		player.gravityScale = 0
		transition.to( player, { delay=0, time=1300, transition=easing.inOutElastic, gravityScale = 1} )
		transition.to( lineGroup, { delay=1, time=200, alpha=1 } )
		
		gameStarted = true
		
		-- destroy gameover scene
		timer.performWithDelay( 10, function() composer.removeScene("gameover"); end)
		-- destroy menu scene
		timer.performWithDelay( 10, function() composer.removeScene("menu"); end)
		
		
		-- start generating lines
		createLine()
		Runtime:addEventListener("enterFrame", moveLine) -- move lines down everyframe
		

		-- start scoreclocka
		local scoreClock = timer.performWithDelay( 1000, updateTime, -1 )
		
		-- touch event
		Runtime:addEventListener( "touch", SwitchSides )
		
		-- player death
		Runtime:addEventListener("enterFrame", playerdeath )
		
		player.postCollision = onPlayerCollision
		player:addEventListener( "postCollision", player )
		
		if settings.devmode == 1 then
			timer.performWithDelay( 250, playerColor, -1 )
		end

	end
end

function scene:hide( event )
	local sceneGroup = self.view

	local phase = event.phase

	if event.phase == "will" then
		-- Called when the scene is on screen and is about to move off screen
		--
		-- INSERT code here to pause the scene
		-- e.g. stop timers, stop animation, unload sounds, etc.)
		
		Runtime:removeEventListener("touch", SwitchSides)
		
	elseif phase == "did" then
		-- Called when the scene is now off screen
	end

end

function scene:destroy( event )

	-- Called prior to the removal of scene's "view" (sceneGroup)
	--
	-- INSERT code here to cleanup the scene
	-- e.g. remove display objects, remove touch listeners, save state, etc.
	local sceneGroup = self.view
	
	-- wait untill scene is destoyed / transition is over
	Runtime:removeEventListener("enterFrame", moveLine)
	Runtime:removeEventListener("enterFrame", playerdeath )
	
	physics.stop()
	
	-- destroy physics stuff
	package.loaded[physics] = nil
	physics = nil
	
	-- dispose audio
	local ST = sounds
	for s=#ST,1,-1 do
		audio.dispose( ST[s] ) ; ST[s] = nil
	end
	
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-----------------------------------------------------------------------------------------

return scene