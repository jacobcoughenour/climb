-----------------------------------------------------------------------------------------
--
-- main.lua
--
-----------------------------------------------------------------------------------------

-- debug info
local versiontext = "null"
if system.getInfo( "environment" ) == "simulator" then
    versiontext = "DevMode Simulator Preview"
else 
	versiontext = string.format("DevMode")
end

-- hide the status bar
display.setStatusBar( display.HiddenStatusBar )
-- include the Corona "composer" module
local composer = require "composer"
-- include "json" library
json = require('json')

-- include Corona's "widget" library
local widget = require "widget"

----------------
-- Variables --
----------------

-- display stuff
screenW, screenH, halfW, halfH = display.contentWidth, display.contentHeight, display.contentWidth*0.5, display.contentHeight*0.5

-- color palette
-- colorname = { r, g, b }
local paletteTable = {
	white = { 250, 250, 250 },
	grey = { 66, 66, 66 },
	black = { 33, 33, 33 },
	blue = { 33, 150, 243 },
	red = { 239, 61, 55 },
	green = { 139, 195, 74 },
}

-- assign palette colors to global variables
-- this could be used to "set themes"
local themeTable = {
	accentcolor = "blue",
	forecolor = "white",
	darkforecolor = "grey",
	bgcolor = "black",
	settingscolorone = "blue",
	settingscolortwo = "red",
	settingscolorthree = "green",
}

-- load sound files
sounds = {
   bork = audio.loadSound( "bork.wav" ),
   swap = audio.loadSound( "swap.wav" )
}

fontface = "Montserrat-Regular"

buttonsize = 72
playersize = 24

local settingsBtn


----------------
-- Get Color --
----------------

function getColor(strthemecolor)
	if strthemecolor == "settingsone" then
		return themeTable.settingscolorone
	elseif strthemecolor == "settingstwo" then
		return themeTable.settingscolortwo
	elseif strthemecolor == "settingsthree" then
		return themeTable.settingscolorthree
	else
		local paletteColorkey = themeTable[strthemecolor] -- get color name from themeTable
		local rgb = paletteTable[paletteColorkey] -- get color value from paletteTable
		local r, g, b = rgb[1], rgb[2], rgb[3] -- split color's table
		local div = 1 / 255 -- convert to percentage
		return r*div, g*div, b*div -- return r, g, b values
	end
end

--------------------------
-- Save and Load File --
--------------------------
local fileiodebug = false

-- Save specified value to specified encrypted file
function saveValue(strFilename, strValue)
	local theFile = strFilename
	local theValue = strValue
	local path = system.pathForFile( theFile, system.DocumentsDirectory )
	local file = io.open( path, "w+" )
	if file then -- If the file exists
		file:write(theValue) -- write strValue to the file.
		io.close(file) -- close the file
		if fileiodebug == true then
			print(string.format ( "saved %s to %s", theValue, strFilename ))
		end
		return true -- confirmation
	end
end

-- Load specified encrypted file, or create new file if it does not exist
function loadValue(strFilename)
	local theFile = strFilename
	local path = system.pathForFile( theFile, system.DocumentsDirectory )
	local file = io.open( path, "r" )
	if file then -- If file exists
		local contents = file:read( "*a" ) -- format content into a string
		io.close( file ) -- close file
		if fileiodebug == true then
			print(string.format ("loaded %s", strFilename ))
		end
		return contents -- Return the table with the file contents
	else
		if fileiodebug == true then
			print(string.format ("%s not found", strFilename ))
		end
		return '' -- Return nothing
	end
end

--------------------------

data = json.decode(loadValue('data.txt'))

if data == nil then -- if no data is loaded / data.txt is empty

	-- default values
	-- they are arranged alphabeticaly by json.encode
	data = {
		currentplayerlives = 5,
		playerlivesmax = 5,
		statTimesPlayed = 0,
		statHighscore = 0
	}
	
	saveValue('data.txt', json.encode(data))
end



settings = json.decode(loadValue('settings.txt'))

if settings == nil then -- if no settings is loaded / settings.txt is empty

	-- default values
	-- they are arranged alphabeticaly by json.encode
	settings = {
		devmode = 0,
		soundmode = 1,
		vibratemode = 0,
		themeaccentcolor = "blue"
	}
	
	saveValue('settings.txt', json.encode(settings))
end

themeTable.accentcolor = settings.themeaccentcolor

--------------------------

-- 'onRelease' event listener for onSettingsBtnRelease
local function onSettingsBtnRelease()
	if composer.getScene("settings") == nil then --wait untill settings scene is destroyed before creating another
		-- go to settings scene
		--composer.gotoScene("settings", "crossFade", 300)
		settingsBtn:setEnabled( false )
		composer.showOverlay( "settings", { isModal = true, effect = "fade", time = 1 } )
	end
	return true	-- indicates successful touch
end

-- Create a background which should appear behind all scenes
local background = display.newRect( 0, 0, display.contentWidth, display.contentHeight )
background:setFillColor( getColor("bgcolor") )
background.anchorX = 0
background.anchorY = 0

-- Debug version text
local debugversiontext = display.newText( versiontext, 2, display.contentHeight, fontface, 12)
debugversiontext:setFillColor( getColor("darkforecolor") )
debugversiontext.anchorX = 0
debugversiontext.anchorY = 1
debugversiontext.alpha = 0

if settings.devmode == 1 then
	debugversiontext.alpha = 1
end

function settingsButtonHide()
	transition.to( settingsBtn, { delay=0, time=300, alpha=0 } )
	settingsBtn:setEnabled( false )
	if settings.devmode == 1 then
		debugversiontext.alpha = 1
	else 
		debugversiontext.alpha = 0
	end
end

function settingsButtonShow()
	transition.to( settingsBtn, { delay=600, time=300, alpha=42/255} )
	settingsBtn:setEnabled( true )
end

-- Settings button
settingsBtn = widget.newButton{
	label="",
	defaultFile = "settingsicon.png",
	overFile = "settingsicon.png",
	emboss = false,
	cornerRadius = 2,
	width=buttonsize*0.5, height=buttonsize*0.5,
	onRelease = onSettingsBtnRelease	-- event listener function
}
settingsBtn.anchorX = 0.5
settingsBtn.anchorY = 0.5
settingsBtn.x = screenW-buttonsize*0.5
settingsBtn.y = screenH-buttonsize*0.5
settingsBtn.alpha = 42/255
	
-- Sort everything in the correct z-index order
local stage = display.getCurrentStage()
stage:insert( background )
stage:insert( composer.stage )
stage:insert( settingsBtn )
stage:insert( debugversiontext )


-- go to the refill scene if out of lives
if data.currentplayerlives < 1 then
	-- go to scene
	composer.gotoScene("refill", "crossFade", 300)
else

	-- load menu screen
	composer.gotoScene( "menu" ) -- set to "menu" before commiting!!!

end