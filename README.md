# TODO #

```
#!lua

[ who, progress ] 
bold = working on it now
```


**Before Release**

-   High score animation [ *DONE* ]
-   Lives regen scene [ *DONE* ]
-   Banner and video ads [ *DONE* ]
-   Better logo / icons [ *DONE* ]
-   Polish animations [ *99%* ]
-   Settings [ *DONE* ]
-   Localize all the things
-   Optimize file size and compress audio
-   Fix bugs [ *33%* ]
-   IOS Port [ On Hold ]
-   color thing [ On Hold ]
-   Play store page [ *80%* ]
-   Final Polish
-   Website thing [ *20%* ]

 

**After Release**

-   Christmas Edition
-   parallel lines idea
-   sound design [ *40%* ]
-   better scaling for text
-   replace images with vector shapes ( drawstring )
-   in-app purchaces ( color change, life packs, max life increase )
-   save data encryption
-   more stats
-   Vibrate mode for haptic feedback [ *DONE* ]
-   Google Play Game Services
    -   Leaderboard
    -   Friends / Circles
	-   marks of friends’ high scores in-game
	-   notifications when friends beat your highscore
    -   Achievements?