-----------------------------------------------------------------------------------------
--
-- gameover.lua
--
-----------------------------------------------------------------------------------------

local composer = require( "composer" )
local scene = composer.newScene()

-- include Corona's "widget" library
local widget = require "widget"

-- include "json" library
json = require('json')

--------------------------------------------

----------------
-- Variables --
----------------

local restartBtn
local refillBtn


-- 'onRelease' event listener for restartBtn
local function onrestartBtnRelease()
	if composer.getScene("game") == nil then --wait untill game scene is destroyed
	
		settingsButtonHide()
	
		-- make sure it's disabled
		restartBtn:setEnabled( false )
	
		-- subtract life
		data.currentplayerlives = data.currentplayerlives - 1
		
		-- save changes to data table
		saveValue('data.txt', json.encode(data))
	
		-- animate button
		restartBtn.alpha=0
		transition.to( restartBtnAniSpin, {delay=0, time=200, rotation=180, alpha=0 } )
		
		-- start pouring
		pourAni.alpha = 0
		local pourdistance = -((halfH+screenH/8-tankborderthickness)-((screenH*0.25*3) + buttonsize*0.5))
		transition.to( pourAni, { delay=0, time=200, height=pourdistance, transition=easing.inExpo } )
		transition.to( pourAni, { delay=0, time=300, alpha=1 } )
		transition.to( lifetankareafill, { delay=0, time=1000, height=(screenH*0.25-tankborderthickness*2)*(lifetankamount-1/16), transition=easing.outExpo } )
		transition.to( restartBtnAniFill, { delay=200, time=950, height=buttonsize, transition=easing.outExpo  } )
		
		-- fade out score group
		transition.to( scoreGroup, { delay=200, time=300, alpha=0 } )
		
		-- move gameover text
		transition.to( gameoverText, { delay=200, time=100, y=-(screenH*0.125)*0.5 } )
		transition.to( gameoverTextbg, { delay=200, time=100, y=-gameoverTextbg.height } )
		
		-- subtract life ( only for display object )
		timer.performWithDelay( 500, function() Lives.text = Lives.text - 1; end)
		
		-- stop pouring
		transition.to( pourAni, { delay=1000, time=150, alpha=0, y=(halfH+screenH*0.125-tankborderthickness)+pourdistance, height=0, transition=easing.inExpo } )
		
		transition.to( lifetankareafill, { delay=200, time=300, alpha=0 } )
		transition.to( lifetankborderGroup, { delay=200, time=300, alpha=0 } )
		transition.to( lifetankGroup, { delay=1300, time=200, alpha=0 } )
		
		
		transition.to( restartBtnAni, { delay=1500, time=1, alpha=0 } )
		transition.to( restartBtnAniFill, { delay=1500, time=200, height=playersize, width=playersize, x=halfW-playersize*0.5, y=screenH/3*2+playersize*0.5 } )
		
		
		-- go to game scene
		timer.performWithDelay( 2000, function() composer.gotoScene("game"); end)
		
		
		return true	-- indicates successful touch
	end
end

-- 'onRelease' event listener for refillBtn
local function onrefillBtnRelease()
	if composer.getScene("game") == nil then --wait untill game scene is destroyed
	
		-- make sure it's disabled
		refillBtn:setEnabled( false )
		
		-- save changes to data table
		--saveValue('data.txt', json.encode(data))
	
		-- transition animation
		
		-- fade out score group
		transition.to( scoreGroup, { delay=200, time=300, alpha=0 } )
		
		-- move gameover text
		transition.to( gameoverText, { delay=200, time=100, y=-(screenH*0.125)*0.5 } )
		transition.to( gameoverTextbg, { delay=200, time=100, y=-gameoverTextbg.height } )

		-- tank border
		transition.to( lifetankbordertop, { delay=500, time=500, y=halfH-screenH*0.25+tankborderthickness*0.5, width=screenW*0.8, transition=easing.inOutExpo } )
		transition.to( lifetankborderright, { delay=500, time=500, x=screenW*0.9-tankborderthickness*0.5, height=screenH*0.5, transition=easing.inOutExpo  } )
		transition.to( lifetankborderbottom, { delay=500, time=500, y=halfH+screenH*0.25-tankborderthickness*0.5, width=screenW*0.8, transition=easing.inOutExpo } )
		transition.to( lifetankborderleft, { delay=500, time=500, x=screenW*0.1+tankborderthickness*0.5, height=screenH*0.5, transition=easing.inOutExpo } )
		
		-- move refill button
		--timer.performWithDelay( 200, function() refillBtn.y=-screenH; end)
		-- transition.to( refillBtnAni, { delay=0, time=200, alpha=1 } )
		-- transition.to( refillBtnAni, { delay=0, time=1000, y=screenH*0.85, width=screenW*0.5, transition=easing.inOutExpo } )
		transition.to( refillBtnGroup, { delay=200, time=500, y=halfH, transition=easing.inExpo } )
		transition.to( refillBtnGroup, { delay=200, time=250, alpha=0 } )
		
		-- load refill scene
		timer.performWithDelay( 1000, function() composer.gotoScene("refill"); end)
		
		
		
		return true	-- indicates successful touch
	end
end


function scene:create( event )
	local sceneGroup = self.view

	-- Called when the scene's view does not exist.
	-- 
	-- INSERT code here to initialize the scene
	-- e.g. add display objects to 'sceneGroup', add touch listeners, etc.
	
	
	-- text background
	gameoverTextbg = display.newRect(0, 0, screenW, screenH*0.125)
	gameoverTextbg.y = -gameoverTextbg.height
	gameoverTextbg:setFillColor( getColor("accentcolor") )
	gameoverTextbg.anchorX = 0
	gameoverTextbg.anchorY = 0
	
	-- "Game Over" Text
	gameoverText = display.newText("Game Over", halfW, -(screenH*0.125)*0.5, fontface, 32) 
	gameoverText:setFillColor( getColor("forecolor") )
	
	
	-- Score Group
	scoreGroup = display.newGroup()
	scoreGroup.anchorX = 0
	scoreGroup.anchorY = 0
	scoreGroup.x = 0
	scoreGroup.y = 0
	scoreGroup.alpha = 0
	
	-- score text
	Score = display.newText("null", screenW*0.25, screenH/5+32, fontface, 40) 
	Score:setFillColor( getColor("accentcolor") )
	scoreGroup:insert( Score )
	
	-- "Score" text
	ScoreText = display.newText("Score", screenW*0.25, screenH/5, fontface, 18) 
	ScoreText:setFillColor( getColor("forecolor") )
	scoreGroup:insert( ScoreText )
	
	-- high score
	HighScore = display.newText("null", screenW*0.25*3, screenH/5+32, fontface, 40)
	HighScore:setFillColor( getColor("accentcolor") )
	scoreGroup:insert( HighScore )
	
	-- "Best" text
	HighScoreText = display.newText("Best", screenW*0.25*3, screenH/5, fontface, 18) 
	HighScoreText:setFillColor( getColor("forecolor") )
	scoreGroup:insert( HighScoreText )
	
	-- "New Best" text
	NewBestText = display.newText("New Best", screenW*0.5, 0, fontface, 20)
	NewBestText:setFillColor( getColor("forecolor") )
	NewBestText.alpha = 0
	scoreGroup:insert( NewBestText )
	
	-- score progress bar 
	ScoreBarBg = display.newRect( 0, 0, screenW/4.5, 4)
	ScoreBarBg.x, ScoreBarBg.y = halfW, screenH/5+32
	ScoreBarBg:setFillColor( getColor("darkforecolor") )
	scoreGroup:insert( ScoreBarBg )
	
	ScoreBar = display.newRect( 0, 0, 0, 4 )
	ScoreBar.x, ScoreBar.y = screenW/2.57, screenH/5+32
	ScoreBar.anchorX = 0
	ScoreBar:setFillColor( getColor("accentcolor") )
	scoreGroup:insert( ScoreBar )
	
	
	-- life tank group
	lifetankGroup = display.newGroup()
	lifetankGroup.anchorX = 0
	lifetankGroup.anchorY = 0
	lifetankGroup.x = 0
	lifetankGroup.y = 0
	lifetankGroup.alpha = 0
	
	lifetankborderGroup = display.newGroup()
	lifetankborderGroup.anchorX = 0
	lifetankborderGroup.anchorY = 0
	lifetankborderGroup.x = 0
	lifetankborderGroup.y = 0
	lifetankborderGroup.alpha = 0
	
	-- border
	tankborderthickness = 2
	lifetankbordertop = display.newRect( halfW, halfH-screenH*0.125+tankborderthickness*0.5, screenW/1.5, tankborderthickness )
	lifetankbordertop:setFillColor( getColor("darkforecolor") )
	lifetankborderright = display.newRect( screenW/6*5-tankborderthickness*0.5, halfH, tankborderthickness, screenH*0.25 )
	lifetankborderright:setFillColor( getColor("darkforecolor") )
	lifetankborderbottom = display.newRect( halfW, halfH+screenH*0.125-tankborderthickness*0.5, screenW/1.5, tankborderthickness )
	lifetankborderbottom:setFillColor( getColor("darkforecolor") )
	lifetankborderleft = display.newRect( screenW/6+tankborderthickness*0.5, halfH, tankborderthickness, screenH*0.25 )
	lifetankborderleft:setFillColor( getColor("darkforecolor") )
	
	--physics.addBody( lifetankbordertop, "static" )
	lifetankborderGroup:insert( lifetankbordertop )
	-- physics.addBody( lifetankborderright, "static" )
	lifetankborderGroup:insert( lifetankborderright )
	-- physics.addBody( lifetankborderbottom, "static" )
	lifetankborderGroup:insert( lifetankborderbottom )
	-- physics.addBody( lifetankborderleft, "static" )
	lifetankborderGroup:insert( lifetankborderleft )
	
	
	-- Lives
	LivesText = display.newText("Lives", halfW, halfH-28, fontface, 18)
	LivesText:setFillColor( getColor("forecolor") )
	lifetankGroup:insert( LivesText )
	
	Lives = display.newText("null", halfW, halfH+8, fontface, 40)
	Lives:setFillColor( getColor("forecolor") )
	lifetankGroup:insert( Lives )
	
	-- life tank group
	lifetankareaGroup = display.newGroup()
	lifetankareaGroup.anchorX = 0
	lifetankareaGroup.anchorY = 0
	lifetankareaGroup.x = 0
	lifetankareaGroup.y = 0
	lifetankareaGroup.alpha = 0
	
	-- life tank fill 
	lifetankareafill = display.newRect( halfW, halfH+screenH*0.125-tankborderthickness, screenW/1.5-tankborderthickness*2, 0 )
	lifetankareafill:setFillColor( getColor("accentcolor") )
	lifetankareafill.anchorX = 0.5
	lifetankareafill.anchorY = 1
	lifetankareaGroup:insert( lifetankareafill )
	
	-- pouring liquid 
	pourAni = display.newRect( halfW, halfH+screenH*0.125-tankborderthickness, buttonsize/5, 0 )
	pourAni:setFillColor( getColor("accentcolor") )
	pourAni.anchorX = 0.5
	pourAni.anchorY = 0
	pourAni.alpha = 1
		
	-- pouring liquid for new best animation 
	ScorePourAni = display.newRect( halfW, ScoreBarBg.y, buttonsize*0.05, 0 )
	ScorePourAni:setFillColor( getColor("accentcolor") )
	ScorePourAni.anchorX = 0.5
	ScorePourAni.anchorY = 0
	ScorePourAni.alpha = 1
	
	-- restart button group
	restartBtnGroup = display.newGroup()
	restartBtnGroup.anchorX = 0
	restartBtnGroup.anchorY = 0
	restartBtnGroup.x = 0
	restartBtnGroup.y = screenH*0.25+buttonsize*0.5
	restartBtnGroup.alpha = 1
	
	-- restart button
	restartBtn = widget.newButton{
		label="",
		defaultFile = "restarticon.png",
		overFile = "restarticon.png",
		emboss = false,
		cornerRadius = 2,
		width=buttonsize, height=buttonsize,
		onRelease = onrestartBtnRelease	-- event listener function
	}
	restartBtn.x = halfW
	restartBtn.y = screenH/4*3
	
	-- refill button group
	refillBtnGroup = display.newGroup()
	refillBtnGroup.anchorX = 0
	refillBtnGroup.anchorY = 0
	refillBtnGroup.x = 0
	refillBtnGroup.y = screenH*0.25+buttonsize*0.5
	refillBtnGroup.alpha = 1
	
	-- refill button
	refillBtn = widget.newButton{
		label="Refill",
		font=fontface,
		fontSize = 28,
		labelYOffset = 0,
		shape="Rect",
		emboss = false,
		strokeWidth = 0,
		labelColor = { default={ getColor("forecolor") }, over={ getColor("bgcolor") } },
		fillColor = { default={ getColor("accentcolor") }, over={ getColor("accentcolor")} },
		width=buttonsize*2, height=buttonsize,
		onRelease = onrefillBtnRelease	-- event listener function
	}
	refillBtn.x = halfW
	refillBtn.y = screenH/4*3
	refillBtn.alpha = 0
	
	refillBtnAni = display.newRect( refillBtn.x, refillBtn.y, buttonsize*2, buttonsize )
	refillBtnAni:setFillColor( getColor("accentcolor") )
	refillBtnAni.alpha = 0
	
	refillBtnGroup:insert( refillBtn )
	refillBtnGroup:insert( refillBtnAni )
	
	restartBtnAni = display.newRect( 0, 0, buttonsize, buttonsize)
	restartBtnAni:setFillColor( getColor("darkforecolor") )
	restartBtnAni.anchorX = 0
	restartBtnAni.anchorY = 0
	restartBtnAni.x = restartBtn.x - buttonsize*0.5
	restartBtnAni.y = restartBtn.y - buttonsize*0.5
	
	restartBtnAniFill = display.newRect( 0, 0, buttonsize, 0)
	restartBtnAniFill:setFillColor( getColor("accentcolor") )
	restartBtnAniFill.anchorX = 0
	restartBtnAniFill.anchorY = 1
	restartBtnAniFill.x = restartBtn.x - buttonsize*0.5
	restartBtnAniFill.y = restartBtn.y + buttonsize*0.5
	
	restartBtnAniSpin = display.newImageRect( "restarticon.png", buttonsize, buttonsize)
	restartBtnAniSpin.anchorX = 0.5
	restartBtnAniSpin.anchorY = 0.5
	restartBtnAniSpin.x = restartBtn.x
	restartBtnAniSpin.y = restartBtn.y
	restartBtnAniSpin.rotation =  0
	
	restartBtnGroup:insert( restartBtnAni )
	restartBtnGroup:insert( restartBtnAniFill )
	restartBtnGroup:insert( restartBtnAniSpin )
	restartBtnGroup:insert( restartBtn )
	
	
	-- all display objects must be inserted into group
	sceneGroup:insert( gameoverTextbg )
	sceneGroup:insert( gameoverText )
	sceneGroup:insert( scoreGroup )
	sceneGroup:insert( lifetankareaGroup )
	sceneGroup:insert( lifetankborderGroup )
	sceneGroup:insert( ScorePourAni )
	sceneGroup:insert( lifetankGroup )
	sceneGroup:insert( restartBtnGroup )
	sceneGroup:insert( refillBtnGroup )
	sceneGroup:insert( pourAni )
end

-- Highscore stuff
local highscorereached = false
local function newbest()
	highscorereached = true
	
	timer.performWithDelay( 2200, function() data.statHighscore = finalscoretime; end )
	timer.performWithDelay( 2200, function() addlife(); end )
	
end

function addlife()
	-- if there is room for another life
	if data.currentplayerlives < data.playerlivesmax then
	
		data.currentplayerlives = data.currentplayerlives + 1

		Lives.text = data.currentplayerlives
		lifetankamount = data.currentplayerlives/data.playerlivesmax
		
		-- add 1 to tank
		transition.to( lifetankareafill, { delay=0, time=500, height=(screenH/4-tankborderthickness*2)*lifetankamount, transition=easing.outExpo } )
	end
end

function scene:show( event )
	local sceneGroup = self.view
	local phase = event.phase
	
	if phase == "will" then
		-- Called when the scene is still off screen and is about to move on screen
	elseif phase == "did" then
		-- Called when the scene is now on screen
		-- 
		-- INSERT code here to make the scene come alive
		-- e.g. start timers, begin animation, play audio, etc.
		
		-- show settings button
		settingsButtonShow()
		
		-- add to stats
		data.statTimesPlayed = data.statTimesPlayed + 1
		
		local outoflives = false
		
		-- get score from game scene
		finalscoretime = composer.getVariable( "gameoverscore" )
		if finalscoretime == nil then -- just incase 
			finalscoretime = 0
		end
		
		if finalscoretime > data.statHighscore then
			newbest()
		elseif data.currentplayerlives < 1 then
			outoflives = true
		end
			
		-- assign variables to display objects
		Score.text = finalscoretime
		HighScore.text = data.statHighscore
		Lives.text = data.currentplayerlives
		lifetankamount = data.currentplayerlives/data.playerlivesmax
		
		
			-- animations
			
			-- move gameover text 
			transition.to( gameoverText, { delay=1, time=1000, y=screenH/8*0.5, transition=easing.outBounce } )
			transition.to( gameoverTextbg, { delay=1, time=400, y=0, transition=easing.outExpo } )
			-- fade in score group
			transition.to( scoreGroup, { delay=400, time=1000, alpha=1, transition=easing.outExpo } )
			-- score bar animation
			ScoreBar.alpha=0.8
			
			-- different animation for reaching highscore
			if highscorereached == true then
				-- score bar fill all the way
				transition.to( ScoreBar, { delay=500, time=1000, alpha=1, transition=easing.outExpo, width=(screenW/4.5)} )
				-- score text
				transition.to( ScoreText, { delay=1000, time=500, alpha=0, transition=easing.outExpo} )
				transition.to( HighScoreText, { delay=1000, time=500, alpha=0, transition=easing.outExpo} )
				-- new best text
				NewBestText.y = screenH*0.25
				transition.to( NewBestText, { delay=1000, time=500, alpha=1} )
				transition.to( NewBestText, { delay=1000, time=1000, y=screenH/5-2, transition=easing.outExpo} )
				-- fade out highscore
				transition.to( HighScore, { delay=1500, time=1000, alpha=0 } )
				-- move score
				transition.to( Score, { delay=2000, time=1000, x=halfW, transition=easing.inOutExpo} )
				-- fade in life tank group
				transition.to( lifetankGroup, { delay=1500, time=500, alpha=1 } )
				transition.to( lifetankborderGroup, { delay=1500, time=500, alpha=1 } )
				transition.to( lifetankareaGroup, { delay=1500, time=500, alpha=1 } )
				-- fill tank
				lifetankareafill.height=(screenH/4-tankborderthickness*2)*lifetankamount
				-- start pouring
				ScorePourAni.alpha = 0
				local scorepourdistance = -(ScoreBarBg.y - lifetankareafill.y) 
				transition.to( ScorePourAni, { delay=2000, time=400, height=scorepourdistance, transition=easing.inExpo } )
				transition.to( ScorePourAni, { delay=2000, time=400, alpha=1 } )
				transition.to( ScoreBar, { delay=2000, time=4, alpha=0 } )
				timer.performWithDelay( 1600, function() ScoreBarBg:setFillColor( getColor("accentcolor")); end)
				transition.to( ScoreBarBg, { delay=2000, time=400, width=0, alpha=0.25, transition=easing.inExpo } )
				-- stop pouring
				transition.to( ScorePourAni, { delay=2350, time=400, alpha=1, y=ScoreBarBg.y+scorepourdistance, height=0, transition=easing.inExpo } )
				-- fade in restart button group
				transition.to( restartBtnGroup, { delay=3000, time=1000, y=0, transition=easing.outExpo } )
				
				-- destroy game scene 
				timer.performWithDelay( 4000, function() composer.removeScene("game"); end)
				
				-- wait until animation is done to save 
				-- save changes to data table
				timer.performWithDelay( 4000, function() saveValue('data.txt', json.encode(data)); end)
			else
				-- score bar fill 
				transition.to( ScoreBar, { delay=500, time=1000, alpha=1, transition=easing.outExpo, width=(screenW/4.5)*(finalscoretime/data.statHighscore) } )
				-- fade in life tank group
				transition.to( lifetankGroup, { delay=500, time=500, alpha=1 } )
				transition.to( lifetankborderGroup, { delay=500, time=500, alpha=1 } )
				transition.to( lifetankareaGroup, { delay=500, time=500, alpha=1 } )
				-- fill tank 
				transition.to( lifetankareafill, { delay=1, time=100, height=(screenH/4-tankborderthickness*2)*lifetankamount, transition=easing.outExpo } )
				
				if outoflives == true then
					-- hide restart button group
					restartBtnGroup.alpha=0
					-- show refill button group
					transition.to( refillBtn, { delay=1000, time=1000, alpha=1 } )
					-- move refill button group
					transition.to( refillBtnGroup, { delay=1000, time=1000, y=0, transition=easing.outExpo } )
				end
				
				-- move restart button group
				transition.to( restartBtnGroup, { delay=1000, time=1000, y=0, transition=easing.outExpo } )
				
				-- destroy game scene 
				timer.performWithDelay( 2000, function() composer.removeScene("game"); end)
				
			end
	end	
end

function scene:hide( event )
	local sceneGroup = self.view
	local phase = event.phase
	
	if event.phase == "will" then
		-- Called when the scene is on screen and is about to move off screen
		--
		-- INSERT code here to pause the scene
		-- e.g. stop timers, stop animation, unload sounds, etc.)
	elseif phase == "did" then
		-- Called when the scene is now off screen
	end	
end

function scene:destroy( event )
	local sceneGroup = self.view
	
	-- Called prior to the removal of scene's "view" (sceneGroup)
	-- 
	-- INSERT code here to cleanup the scene
	-- e.g. remove display objects, remove touch listeners, save state, etc.
	
	if restartBtn then
		restartBtn:removeSelf()	-- widgets must be manually removed
		restartBtn = nil
	end
	
	if refillBtn then
		refillBtn:removeSelf()	-- widgets must be manually removed
		refillBtn = nil
	end
	
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-----------------------------------------------------------------------------------------

return scene